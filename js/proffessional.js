(function(){
  'use strict';
  
  var app = angular.module('myApp',[]);
  
  app.controller('profileApi',[
    '$scope',
    'testingService',
    function(
      $scope,
      testingService
      ){
      testingService.firstApi().then(function(result){
       $scope.data = result.data;
     },function(response){
       $scope.data = response;
     });
      
    }]);
  
  app.factory('testingService',['$http','$q',function($http, $q){
    return{
      firstApi : firstApi
    }
    
    function firstApi(){
      var deferred = $q.defer();
      var req = {
        method : "GET",
        url :'http://www.buildzar.com/api/v1/professional/microsite',
        params:{id:13},
      }
      $http(req)
      .then(function(result){
        deferred.resolve(result);
      },function(response){
        deferred.reject(response);
      });
      return deferred.promise;
    }
  }]);

  var my = {
    "Find Dealers": "Find Dealers",
    "Find Architects & Designers" : "Find Architects & Designers",
    "Find Contractors": "Find Contractors",
    "Best Rates": "Best Rates",
    "Beztimate": "Beztimate",
    "Room Visualizer": "Room Visualizer",
    "Room Guide":"Room Guide",
    "Blogs": "Blogs"

  };

  var my1 = {
    "Free Listing": "Free Listing",
    "Featured Listing" : "Featured Listing",
    "Banner Advertising": "Banner Advertising",
    "Leads Database": "Leads Database"

  };


  var my2 = {
   "Free Listing": "Free Listing",
   "Portfolio Listing" : "Portfolio Listing",
   "Featured Listing" : "Featured Listing",
   "Banner Advertising": "Banner Advertising",
   "Leads Database": "Leads Database"
 };


 var my3 = {
   "Free Listing": "Free Listing",
   "Portfolio Listing" : "Portfolio Listing",
   "Featured Listing" : "Featured Listing",
   "Banner Advertising": "Banner Advertising",
   "Leads Database": "Leads Database"
 };

 var my4 ={
   "About Us": "About Us",
   "Careers" : "Careers",
   "Privacy Policy" : "Privacy Policy",
   "Terms of Use": "Terms of Use",
   "Intelluctual Property": "Intelluctual Property",
   "Rights":"Rights",
   "Contact Us":"Contact Us"
 };
 app.controller('myCtrl',function($scope){
  $scope.items=my;
  $scope.items1=my1;
  $scope.items2=my2;
  $scope.items3=my3;
  $scope.items4=my4;
});

 app.controller("dealerName", function($scope){
   $scope.name=["Dealer1", "Dealer2", "Dealer3"];
 });

 app.controller("placeSearch", function($scope){
  $scope.city=["New Delhi","Mumbai","Goa"]
});
 
 app.controller("imagesLogo", function($scope){
  var images={
   myVar:"images/logo.png"
 };
 $scope.images=images; 
});

 app.controller("servicesProvides", function($scope){
  $scope.design=[" Planning & layout","Interior design","Costing & Estimation BOQ","Landscape design","Site visit","Structural design"]
});


 
})();