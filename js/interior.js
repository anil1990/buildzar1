(function(){
 'use strict';

     var app = angular.module('myApp',[]);
  
  app.controller('interiorApi',[
    '$scope',
    'testingService',
    function(
    $scope,
     testingService
    ){
      
      testingService.firstApi().then(function(result){
         $scope.data = result.data;
      },function(response){
         $scope.data = response;
      });
      
  }]);
  
  app.factory('testingService',['$http','$q',function($http, $q){
      return{
        firstApi : firstApi
      }
      
      function firstApi(){
        var deferred = $q.defer();
        var req = {
          method : "GET",
          url : 'http://www.buildzar.com//api/v1/professional/searchResults',
          params:{
            city: 'gurgaon',
            shopName:'architect-a1'
          }
        }
        $http(req)
          .then(function(result){
            deferred.resolve(result);
          },function(response){
            deferred.reject(response);
          });
        return deferred.promise;
      }
  }]);
 
})();
